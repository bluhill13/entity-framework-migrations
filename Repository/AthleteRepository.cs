﻿using Entity_Framework_Migrations.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations.Repository
{
    public static class AthleteRepository
    {
        private readonly static List<Athlete> emptyCollection = new List<Athlete>();
        /// <summary>
        /// Gets all athletes in the database and returns a list
        /// </summary>
        /// <returns></returns>
        public static List<Athlete> GetAllAthletes()
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var athletes = context.Athletes.Select(s => s).ToList();
                    return athletes;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }
            
            return emptyCollection;
        }
        /// <summary>
        /// Gets all athletes and their coach object if they have any assigned
        /// </summary>
        /// <returns></returns>
        public static List<Athlete> GetAllAthletesWithCoach()
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var athletes = context.Athletes.Select(s => s).Include(s => s.Coach).ToList();
                    return athletes;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }

            return emptyCollection;
        }
        public static List<Athlete> GetAthleteById(int id)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var athletes = context.Athletes.Include(s => s.Coach).Where(p => p.Id == id).ToList();
                    return athletes;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }

            return emptyCollection;
        }
        /// <summary>
        /// Updates athlete coach id to e new one, 
        /// effectively assigning a coach to a athelte
        /// </summary>
        /// <param name="a"></param>
        /// <param name="coachId"></param>
        public static void UpdateAthlete(Athlete a, int coachId)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var athletes = context.Athletes.Where(p => p.Id == a.Id).ToList();
                    athletes.First().CoachId = coachId;
                    context.Athletes.Update(athletes.First());
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }
        }
        /// <summary>
        /// Deletes an athlete by id from the database
        /// </summary>
        /// <param name="a"></param>
        public static void DeleteAthlete(Athlete a)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    context.Athletes.Remove(a);
                    context.SaveChanges();
                    Console.WriteLine($"Athelte {a.First_name} {a.Last_name}, have been deleted");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }
        }
        /// <summary>
        /// Adds a new athlete to database
        /// </summary>
        /// <param name="a"></param>
        public static void CreateAthlete(Athlete a)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    context.Athletes.Add(a);
                    context.SaveChanges();
                    Console.WriteLine("New athlete sucessfully added to database"); 
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }
        }
    }
}
