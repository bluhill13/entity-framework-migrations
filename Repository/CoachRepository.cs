﻿using Entity_Framework_Migrations.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations.Repository
{
    public static class CoachRepository
    {
        private readonly static List<Coach> emptyList = new List<Coach>();
        /// <summary>
        /// Gets all coaches in the database and returns a list
        /// </summary>
        /// <returns></returns>
        public static List<Coach> GetAllCoaches()
        {
            
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var temp = context.Coaches.Select(s => s);
                    var coaches = temp.ToList();
                    return coaches;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }
            
            return emptyList;
        }
        /// <summary>
        /// Returns a coach by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Coach> GetCoachById(int id)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var coaches = context.Coaches.Include(s => s.athletes).Where(p => p.Id == id).ToList();
                    return coaches;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }

            return emptyList;
        }
    }
}
