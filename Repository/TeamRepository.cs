﻿using Entity_Framework_Migrations.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations.Repository
{
    public static class TeamRepository
    {
        private readonly static List<Team> emptyList = new List<Team>();
        /// <summary>
        /// Gets all Teams in the database and returns a list of team objects
        /// </summary>
        /// <returns></returns>
        public static List<Team> GetAllTeams()
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var temp = context.Teams.Select(s => s);
                    var teams = temp.ToList();
                    return teams;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }

            return emptyList;
        }
        /// <summary>
        /// Returns a coach by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Team> GetTeamById(int id)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var team = context.Teams.Where(p => p.Id == id).ToList();
                    return team;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }

            return emptyList;
        }
    }
}
