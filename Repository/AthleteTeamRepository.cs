﻿using Entity_Framework_Migrations.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations.Repository
{
    public static class AthleteTeamRepository
    {
        private readonly static List<AthleteTeam> emptyList = new List<AthleteTeam>();
        /// <summary>
        /// Gets all athlete id's for a given teamId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<AthleteTeam> GetTeamAndItsAthletesById(int id)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var athletes = context.athleteTeams.Where(p => p.TeamId == id).ToList();
                    return athletes;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }
            
            return emptyList;
        }
        /// <summary>
        /// adds a new athlete / team combination to the table ensuring the many to many relationship is defined
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="athleteId"></param>
        public static void AddNewAthleteTeam(int teamId, int athleteId)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    context.athleteTeams.Add(new AthleteTeam { AthleteId = athleteId, TeamId = teamId });
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong! {e.Message}");
            }
        }
        /// <summary>
        /// Performs a search to verify that the team /athlete combination does not exist
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="athleteId"></param>
        /// <returns>true if combination exists, false if not</returns>
        public static bool CombinationDoesNotExist(int teamId, int athleteId)
        {
            try
            {
                using (TeamManagerDbContext context = new TeamManagerDbContext())
                {
                    var athletes = context.athleteTeams.Select(s => s).Where(p => p.TeamId == teamId).Where(q => q.AthleteId == athleteId);
                    if (athletes != null) return true;
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong, combination probably does not exist! {e.Message}");
                return false;
            }
        }
    }
}
