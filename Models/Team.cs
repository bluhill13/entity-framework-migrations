﻿using System.Collections.Generic;

namespace Entity_Framework_Migrations.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Team_name { get; set; }
        public string? Sport { get; set; }
        public ICollection<AthleteTeam> athleteTeam { get; set; }
    }
}
