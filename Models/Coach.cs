﻿using System.Collections.Generic;

namespace Entity_Framework_Migrations.Models
{
    public class Coach
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string? Sport { get; set; }
        public int? Age { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        //Generates the one to many relationship for athletes
        public ICollection<Athlete> athletes { get; set; }
    }
}
