﻿using Microsoft.EntityFrameworkCore;

namespace Entity_Framework_Migrations.Models
{
    public class TeamManagerDbContext : DbContext
    {
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Athlete> Athletes { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<AthleteTeam> athleteTeams { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7277\\SQLEXPRESS;Initial Catalog=TeamManagerDb;Integrated Security=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AthleteTeam>()
                .HasKey(bc => new { bc.AthleteId, bc.TeamId });
            modelBuilder.Entity<AthleteTeam>()
                .HasOne(bc => bc.Athlete)
                .WithMany(b => b.athleteTeam)
                .HasForeignKey(bc => bc.AthleteId);
            modelBuilder.Entity<AthleteTeam>()
                .HasOne(bc => bc.Team)
                .WithMany(c => c.athleteTeam)
                .HasForeignKey(bc => bc.TeamId);
        }
    }
}
