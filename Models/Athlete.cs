﻿using System.Collections.Generic;

namespace Entity_Framework_Migrations.Models
{
    public class Athlete
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public int? Age { get; set; }
        public int? CoachId { get; set; }
        public Coach Coach { get; set; }
        public ICollection<AthleteTeam> athleteTeam { get; set; }
    }
}
