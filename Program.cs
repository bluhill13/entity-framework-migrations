﻿using System;

namespace Entity_Framework_Migrations
{
    static class Program
    {
        public static bool validChoice { get; set; }
        public static bool exitProgram { get; set; } = true;
        static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("\tTeam manager 2.0\n");
                Menu();
            } while (exitProgram);
        }
        /// <summary>
        /// Menu promting user to make a choice, will be called in main until user selects exit.
        /// </summary>
        public static void Menu()
        {
            validChoice = false;
            do
            {
                var choice = AskAndGetNumberInputFromUser("\tMenu\n " +
                                                    "\t'1' See all Coaches\n" +
                                                    "\t'2' See all Athletes\n" +
                                                    "\t'3' Assign or update an athlete's coach\n" +
                                                    "\t'4' See all athletes for a coach\n" +
                                                    "\t'5' See all athletes for a given team\n" +
                                                    "\t'6' Add a athlete to a team\n" +
                                                    "\t'7' Add a new athlete\n" +
                                                    "\t'8' Delete a athlete\n" +
                                                    "\t'9' Write JSON to console from all athletes\n" +
                                                    "\t'0' Exit program\n" +
                                                    "\tCommand: ");
                var Id = 0;
                validChoice = false;
                switch (choice)
                {
                    case 1:
                        Service.CoachService.DisplayAllCoaches();
                        break;
                    case 2:
                        Service.AthleteService.DisplayAllAthletes();
                        break;
                    case 3:
                        Service.AthleteService.DisplayAllAthletes();
                        var athleteId = AskAndGetNumberInputFromUser($"Select a athlete by id: ");
                        Service.AthleteService.DisplayAthleteById(athleteId, true);
                        var displayCoaches = AskAndGetNumberInputFromUser($"Display coaches? '1' for yes '2' for no");
                        if (displayCoaches == 1) Service.CoachService.DisplayAllCoaches();
                        var coachId = AskAndGetNumberInputFromUser($"Write a new coach id: ");
                        Service.AthleteService.AssignAthleteToCoach(coachId);
                        //redisplay athlete with coach information
                        Service.AthleteService.DisplayAthleteById(athleteId, true);
                        break;
                    case 4:
                        Service.CoachService.DisplayAllCoaches();
                        Id = AskAndGetNumberInputFromUser($"Select a coach by id: ");
                        Service.CoachService.DisplayCoachAndHisAthletes(Id);
                        break;
                    case 5:
                        Service.TeamService.DisplayAllTeams();
                        Id = AskAndGetNumberInputFromUser($"Select a team by id: ");
                        Service.AthleteTeamService.DisplayAllAtheletesForATeam(Id);
                        break;
                    case 6:
                        Service.AthleteService.DisplayAllAthletes();
                        var athleteIdToAdd = AskAndGetNumberInputFromUser($"Select a athlete by id: ");
                        Service.AthleteService.DisplayAthleteById(athleteIdToAdd, true);
                        Service.TeamService.DisplayAllTeams();
                        Id = AskAndGetNumberInputFromUser($"Select a team id to add athlete to it: ");
                        Service.AthleteTeamService.AddAthleteToTeam(Id, athleteIdToAdd);
                        break;
                    case 7:
                        Service.AthleteService.CreateAthlete();
                        break;
                    case 8:
                        Service.AthleteService.DisplayAllAthletes();
                        Id = AskAndGetNumberInputFromUser($"Select a athlete by id to delete: ");
                        Service.AthleteService.DeleteAthlete(Id);
                        break;
                    case 9:
                        Service.AthleteService.SerializeAndDisplayAthlets();
                        break;
                    case 0:
                        exitProgram = false;
                        break;
                    default:
                        Console.WriteLine("You have not choosen a valid number!");
                        validChoice = true;
                        break;

                }
            } while (validChoice);
        }
        /// <summary>
        /// Ask's the user for input and displays a message to describe the purpose of given input.
        /// Handles an eventual formatting exception and other error handling.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static int AskAndGetNumberInputFromUser(string message)
        {
            Console.Write(message);
            bool validInput = false;
            string input = null;
            int size = 0;
            do
            {
                input = Console.ReadLine();

                validInput = IsDigitsOnly(input);
                if (!validInput)
                {
                    Console.WriteLine("Input is not valid! Only accepted input is numbers, try again");
                }
                else
                {   //Not really nessesary to have this try catch here, other than failsafing the size of the input from user
                    try
                    {
                        size = Int32.Parse(input);
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine($"Error while converting string to int, exception message: {ex.Message}");
                    }
                }
            } while (!validInput);

            return size;
        }
        /// <summary>
        /// Validates that string only have numbers in it. 
        /// </summary>
        /// <param name="str">user input as string</param>
        /// <returns>false if there is non numbers present in input string</returns>
        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
    }
}
