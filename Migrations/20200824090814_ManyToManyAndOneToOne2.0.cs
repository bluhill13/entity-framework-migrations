﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity_Framework_Migrations.Migrations
{
    public partial class ManyToManyAndOneToOne20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AthleteTeam_Athletes_AthleteId",
                table: "AthleteTeam");

            migrationBuilder.DropForeignKey(
                name: "FK_AthleteTeam_Team_TeamId",
                table: "AthleteTeam");

            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_Team_TeamId",
                table: "Coaches");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Team",
                table: "Team");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AthleteTeam",
                table: "AthleteTeam");

            migrationBuilder.RenameTable(
                name: "Team",
                newName: "Teams");

            migrationBuilder.RenameTable(
                name: "AthleteTeam",
                newName: "athleteTeams");

            migrationBuilder.RenameIndex(
                name: "IX_AthleteTeam_TeamId",
                table: "athleteTeams",
                newName: "IX_athleteTeams_TeamId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Teams",
                table: "Teams",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_athleteTeams",
                table: "athleteTeams",
                columns: new[] { "AthleteId", "TeamId" });

            migrationBuilder.AddForeignKey(
                name: "FK_athleteTeams_Athletes_AthleteId",
                table: "athleteTeams",
                column: "AthleteId",
                principalTable: "Athletes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_athleteTeams_Teams_TeamId",
                table: "athleteTeams",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_Teams_TeamId",
                table: "Coaches",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_athleteTeams_Athletes_AthleteId",
                table: "athleteTeams");

            migrationBuilder.DropForeignKey(
                name: "FK_athleteTeams_Teams_TeamId",
                table: "athleteTeams");

            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_Teams_TeamId",
                table: "Coaches");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Teams",
                table: "Teams");

            migrationBuilder.DropPrimaryKey(
                name: "PK_athleteTeams",
                table: "athleteTeams");

            migrationBuilder.RenameTable(
                name: "Teams",
                newName: "Team");

            migrationBuilder.RenameTable(
                name: "athleteTeams",
                newName: "AthleteTeam");

            migrationBuilder.RenameIndex(
                name: "IX_athleteTeams_TeamId",
                table: "AthleteTeam",
                newName: "IX_AthleteTeam_TeamId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Team",
                table: "Team",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AthleteTeam",
                table: "AthleteTeam",
                columns: new[] { "AthleteId", "TeamId" });

            migrationBuilder.AddForeignKey(
                name: "FK_AthleteTeam_Athletes_AthleteId",
                table: "AthleteTeam",
                column: "AthleteId",
                principalTable: "Athletes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AthleteTeam_Team_TeamId",
                table: "AthleteTeam",
                column: "TeamId",
                principalTable: "Team",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_Team_TeamId",
                table: "Coaches",
                column: "TeamId",
                principalTable: "Team",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
