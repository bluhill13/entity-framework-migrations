﻿using Entity_Framework_Migrations.Models;
using Entity_Framework_Migrations.Repository;
using System;

namespace Entity_Framework_Migrations.Service
{
    public static class AthleteTeamService
    {
        /// <summary>
        /// Displays a Team by id and also displays the athletes is assigned to that team
        /// </summary>
        /// <param name="TeamId"></param>
        public static void DisplayAllAtheletesForATeam(int TeamId)
        {
            var AthleteTeams = AthleteTeamRepository.GetTeamAndItsAthletesById(TeamId);
            TeamService.DisplayTeamById(TeamId);
            Console.WriteLine("\tAthletes on this team");
            foreach (AthleteTeam a in AthleteTeams)
            {
                AthleteService.DisplayAthleteById(a.AthleteId, false);
            }
        }

        /// <summary>
        /// adds a team athlete pair to the many to many relationship table athleteteam
        /// performs a check first to verify the combination does not already exist
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="athleteId"></param>
        public static void AddAthleteToTeam(int teamId, int athleteId)
        {
            if (AthleteTeamRepository.CombinationDoesNotExist(teamId, athleteId))
            {
                AthleteTeamRepository.AddNewAthleteTeam(teamId, athleteId);
                Console.WriteLine($"Sucess! Athlete with Id: {athleteId} was added to Team with Id: {teamId}");
            }
            else
            {
                Console.WriteLine("Athlete is already part of the team");
            }
        }
    }
}
