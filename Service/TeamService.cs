﻿using Entity_Framework_Migrations.Models;
using Entity_Framework_Migrations.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations.Service
{
    public static class TeamService
    {
        /// <summary>
        /// Displays all coaches from database and displays them
        /// </summary>
        public static void DisplayAllTeams()
        {
            List<Team> coaches = TeamRepository.GetAllTeams();
            foreach (Team c in coaches)
            {
                Console.WriteLine($"\t Id: {c.Id} \n\tSport: {c.Sport} Team name: {c.Team_name} ");
            }
        }
        /// <summary>
        /// Displays a team by id
        /// </summary>
        /// <param name="teamId"></param>
        public static void DisplayTeamById(int teamId)
        {
            var Teams = TeamRepository.GetTeamById(teamId);
            var team = Teams.First();
            Console.WriteLine($"\t\tTEAM \n\t Id: {team.Id} \n\t Sport: {team.Sport}, Team name: {team.Team_name}\n");
        }

    }
}
