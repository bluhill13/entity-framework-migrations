﻿using Entity_Framework_Migrations.Models;
using Entity_Framework_Migrations.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations.Service
{
    public static class CoachService
    {
        /// <summary>
        /// Displays all coaches from database and displays them
        /// </summary>
        public static void DisplayAllCoaches()
        {
            List<Coach> coaches = CoachRepository.GetAllCoaches();
            foreach (Coach c in coaches)
            {
                int count = 0;
                if (c.athletes != null) count = c.athletes.Count; 
                Console.WriteLine($"\t Id: {c.Id} \n\t Name: {c.First_name} {c.Last_name} Age: {c.Age}\n\t Coaching for: {count} number of athletes");
            }
        }

        /// <summary>
        /// Displays a coach by id and also displays the athlete is assigned to that coach
        /// </summary>
        /// <param name="athleteId"></param>
        public static void DisplayCoachAndHisAthletes(int athleteId)
        {
            var Athlete = CoachRepository.GetCoachById(athleteId);
            var coach = Athlete.First();
            
            Console.WriteLine($"\tCoach for player:\n\t\tId: {coach.Id} \n\t\tName: {coach.First_name} {coach.Last_name} Age: {coach.Age}\n\t\n");
            Console.WriteLine("\tAthletes trained by this coach are:");
            var athletes = coach.athletes.ToList();
            foreach(Athlete a in athletes)
            {
                Console.WriteLine($"\t\t Id: {a.Id} \n\t\t Name: {a.First_name} {a.Last_name} Age: {a.Age}\n");
            } 
        }

    }
}
