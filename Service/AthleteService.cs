﻿using Entity_Framework_Migrations.Models;
using Entity_Framework_Migrations.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations.Service
{
    public static class AthleteService
    {
        private static Athlete tempAthlete = new Athlete();
        /// <summary>
        /// Displays all athleates from database and displays them
        /// </summary>
        public static void DisplayAllAthletes()
        {
            List<Athlete> coaches = AthleteRepository.GetAllAthletes();
            foreach (Athlete c in coaches)
            {
                Console.WriteLine($"\t Id: {c.Id} \n\t Name: {c.First_name} {c.Last_name} Age: {c.Age}\n");
            }
        }
        /// <summary>
        /// Displays an athlete by id and also displays the coach the athlete is assigned to
        /// </summary>
        /// <param name="athleteId"></param>
        public static void DisplayAthleteById(int athleteId, bool displayCoach)
        {
            var Athlete = AthleteRepository.GetAthleteById(athleteId);
            tempAthlete = Athlete.First();
            Console.WriteLine($"\t Id: {tempAthlete.Id} \n\t Name: {tempAthlete.First_name} {tempAthlete.Last_name} Age: {tempAthlete.Age}\n");
            if (tempAthlete.Coach != null && displayCoach) Console.WriteLine($"\tCoach for player:\n\t\tId: {tempAthlete.Coach.Id} \n\t\tName: {tempAthlete.Coach.First_name} {tempAthlete.Coach.Last_name} Age: {tempAthlete.Coach.Age}\n\t");
        }

        public static void AssignAthleteToCoach(int coachId)
        {
            AthleteRepository.UpdateAthlete(tempAthlete, coachId);
        }
        /// <summary>
        /// Promts user to enter forstname, lastname and age of new athlete
        /// then sends new athlete object to be added to the database
        /// </summary>
        public static void CreateAthlete()
        {
            Athlete athlete = new Athlete();
            Console.Write("First name of athlete: ");
            athlete.First_name = Console.ReadLine();
            Console.Write("Last name of athlete: ");
            athlete.Last_name = Console.ReadLine();
            athlete.Age = Program.AskAndGetNumberInputFromUser("Enter age of athlete: ");
            AthleteRepository.CreateAthlete(athlete);
        }
        

        /// <summary>
        /// Deletes a athlete with a given Id from the database
        /// </summary>
        /// <param name="athleteId"></param>
        public static void DeleteAthlete(int athleteId)
        {
            var athlete = AthleteRepository.GetAthleteById(athleteId);
            AthleteRepository.DeleteAthlete(athlete.First());
        }
        /// <summary>
        /// Serializes objects retrived from the database and print them in console,
        /// referenceloophandlign is set to ignore to not crach code, and be able to also
        /// write coach and his other athltes to consoll.
        /// </summary>
        public static void SerializeAndDisplayAthlets()
        {
            List<Athlete> athletes = AthleteRepository.GetAllAthletesWithCoach();
            string jsonString;
            foreach (Athlete a in athletes)
            {
                jsonString = JsonConvert.SerializeObject(a, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
                Console.WriteLine(jsonString);
            }
            
        }
    }
}
